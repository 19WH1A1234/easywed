import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ServiceCusService {
  // userRegister(userdetails: any) {
  //   throw new Error('Method not implemented.');
  // }
  userLoggedIn:boolean;
  constructor(private httpClient:HttpClient) {           
    this.userLoggedIn = true;            //initially set to false
  }

  getUserLoggedIn(){
    return this.userLoggedIn;
  } 

  setUserLoggedIn(){
    this.userLoggedIn=true;
  }
  showAllUsers(){
    return this.httpClient.get('http://localhost:3000/fetch');
  };
  // userRegister(customers:any){
  //   return this.httpClient.post('http://localhost:3000/register',customers);
  // }
  registerCus(customers:any){
    return this.httpClient.post('http://localhost:3000/register',customers);
  }
  getUserByEmailAndPassword(loginForm:any){
    return this.httpClient.get('http://localhost:3000/login/'+ loginForm.email+"/"+loginForm.password).toPromise();
  }
}
