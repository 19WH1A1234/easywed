//import express module
let express = require("express");
//import body-parser module
let bodyparser = require("body-parser");


let cors = require("cors");

//create the rest object
let app=express();
//enable ports communication

app.use(cors());

//set the JSON as MIME type
app.use(bodyparser.json());
//read the JSON
app.use(bodyparser.urlencoded({extended:false}));

//use login module
// app.use("/login",require("./login/login"));
//use register module
app.use("/register",require("./register/register"));
//use register module
// app.use("/update",require("./update/update"));
//use register module
// app.use("/delete",require("./delete/delete"));

//use fetch module
app.use("/fetch",require("./fetch/fetch"));
//assign the port no
app.listen(3000);
console.log("server listening the port no.3000");
