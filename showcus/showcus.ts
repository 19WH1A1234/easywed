// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-showcust',
//   templateUrl: './showcust.component.html',
//   styleUrls: ['./showcust.component.css']
// })
// export class ShowcustComponent implements OnInit {

//   constructor() { }

//   ngOnInit(): void {
//   }

// }





import { Component, OnInit } from '@angular/core';
import { ServiceCusService } from '../service-cus.service';

@Component({
  selector: 'app-showcust',
  templateUrl: './showcust.component.html',
  styleUrls: ['./showcust.component.css']
})
export class ShowcustComponent implements OnInit {
  customers:any;

  constructor(private service: ServiceCusService) { 
    // this.employee = [{EmpId: '1213', EmpName: 'Shivani', DOJ: '12/25/2011', Sal: '10000', Gender:'Female'}, 
    // {EmpId: '1234', EmpName: 'Shreeya Reddy', DOJ: '2/3/2011', Sal: '10000', Gender:'Female'}, 
    // {EmpId: '1215', EmpName: 'Selvi Reddy', DOJ: '11/12/2012', Sal: '10000', Gender:'Female'},
    // {EmpId: '1227', EmpName: 'Shravya', DOJ: '1/12/2012', Sal: '10000', Gender:'Female'}];
  }

  ngOnInit(): void {
    this.service.showAllUsers().subscribe((result: any)=> {console.log(result); this.customers=result;});
  }

}
