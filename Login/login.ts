import { Component, OnInit } from '@angular/core';
import { RegisterComponent } from '../register/register.component';
import { NavigationEnd, Router } from '@angular/router';
import { ServiceCusService } from '../service-cus.service';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email : any;
  password : string;

  constructor(private router:Router,private service:ServiceCusService) {
    this.email = '';
    this.password = '';
  }

  ngOnInit(): void {
  }
  loginSubmit(loginForm:any) : void {
    console.log(loginForm.email);
    console.log(loginForm.password);
    this.email = loginForm.email;
    this.password=loginForm.password;
    this.service.getUserEmailAndPassword(loginForm).subscribe((result:any)=>this.checkCred(result));
  }
  newLogin(){
    console.log("Working");
    this.router.navigateByUrl('/register')
   }
   checkCred(users:any){
     console.log(users.email);
     console.log(users.password);
     console.log(this.email);
     console.log(this.password);
    if(users.email==this.email || users.password==this.password){
         alert("Logged in");
         this.router.navigateByUrl('/homepage');
    }else{
         alert("Incorrect password or email");
    }
   }
}

function result(result: any, any: any) {
  throw new Error('Function not implemented.');
}
