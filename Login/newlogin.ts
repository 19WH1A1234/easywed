import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceCusService } from '../service-cus.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email : string;
  password : string;
  customers : any;
  
  constructor(private router : Router,private service:ServiceCusService,private httpClient:HttpClient) { 
    this.email='';
    this.password='';
  }

  ngOnInit(): void {
  }


async loginSubmit(loginForm : any){
  if(loginForm.email === "HR" && loginForm.password === "HR"){
  this.service.setUserLoggedIn();
  
  }
  else{
    await this.service.getUserByEmailAndPassword(loginForm).then((result:any)=>{
      this.customers = result;
    });
    console.log("Data Fetched : ",this.customers);
    
    this.service.setUserLoggedIn();
    if(this.customers != null){
      alert("Logged in successfully!!");
      this.router.navigate(['venue']) ;
    }
    else{
      alert('Invalid User');
    }
  }
}
}
