import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { ServiceCusService } from '../service-cus.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  customers : any;
  
  constructor(private httpClient:HttpClient,private service:ServiceCusService) { 
    this.customers={username:' ',email:' ',password : ' ',phoneno : ' '};
  }

  ngOnInit(): void {
  }
  checkPwd(){
    console.log("Check");

  }
  async signUp(registerForm:any){
    // alert(this.email);
    console.log(registerForm.password);
    console.log(registerForm.c_password);
    this.customers.username = registerForm.username;
    this.customers.email = registerForm.email;
    this.customers.password = registerForm.password;
    this.customers.phoneno= registerForm.phoneno;
    console.log(this.customers.username);
    console.log(this.customers.email);
    console.log(this.customers.password);
    console.log(this.customers.phoneno);

    if(registerForm.password===registerForm.c_password){
      // this.service.signUp(this.userdetails).subscribe();
      this.userRegister();
      alert("Registered");

    }
    else{
      alert("Check your password");
    }
  }
  // userRegister(){
  //   this.service.userRegister(this.userdetails).subscribe();
  // }
  userRegister(){
    this.service.userRegister(this.customers).subscribe();
    // this.service.showAllUsers().subscribe();
    alert("Registered");
  }

}
